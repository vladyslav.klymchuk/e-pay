package ua.klymchuk.vladyslav.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder auth, JdbcTemplate template) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(template.getDataSource())
                .usersByUsernameQuery("SELECT u.login, u.password, u.available FROM \"users\" AS u WHERE UPPER(u.login) LIKE UPPER(?)")
                .authoritiesByUsernameQuery("SELECT u.login, u.role FROM \"users\" AS u WHERE UPPER(u.login) LIKE UPPER(?)")
                .passwordEncoder(new BCryptPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers("/").permitAll()
                    .antMatchers("/login").anonymous()
                    .antMatchers("/user/registration").permitAll()
                    .antMatchers("/logout").permitAll()
                    .antMatchers("/*").authenticated()
                .and()
                .formLogin()
                .and()
                .logout()
                    .invalidateHttpSession(true)
                    .clearAuthentication(true)
                    .deleteCookies("JSESSIONID")
                    .logoutSuccessUrl("/")
                .and()
                .httpBasic();
        http.csrf().disable();
    }
}
