package ua.klymchuk.vladyslav.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.klymchuk.vladyslav.form.AccountEditForm;
import ua.klymchuk.vladyslav.service.AccountService;

@Controller
@RequestMapping("/account")
public class AccountController {
    private final AccountService service;

    @Autowired
    public AccountController(AccountService service) {
        this.service = service;
    }


    @PostMapping
    public ModelAndView save(@ModelAttribute("form") AccountEditForm form) {
        service.save(form);

        return new ModelAndView("redirect:user")
                .addObject("message", "Account add done");
    }
}
