package ua.klymchuk.vladyslav.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import ua.klymchuk.vladyslav.form.PaymentRegistrationForm;
import ua.klymchuk.vladyslav.service.PaymentService;

@RestController
@RequestMapping("/payment")
public class PaymentController {
    private final PaymentService service;

    @Autowired
    public PaymentController(PaymentService service) {
        this.service = service;
    }

    @PostMapping
    public ModelAndView add(@ModelAttribute("form") PaymentRegistrationForm form) {
        final ModelAndView model = new ModelAndView("redirect:user");
        if (service.save(form)) {
            model.addObject("message", "Payment done");
        } else {
            model.addObject("message", "Payment failed");
        }

        return model;
    }
}