package ua.klymchuk.vladyslav.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.klymchuk.vladyslav.domain.User;
import ua.klymchuk.vladyslav.form.UserRegistrationForm;
import ua.klymchuk.vladyslav.service.UserService;

@Controller
@RequestMapping("/user/registration")
public class RegistrationController {
    private final UserService userService;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ModelAndView registration() {
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return new ModelAndView("redirect:/");
        }

        return new ModelAndView("registration")
                .addObject("massage", "Please registration new user!");
    }

    @PostMapping
    public ModelAndView registration(@ModelAttribute("form") UserRegistrationForm form) {
        final ModelAndView model = new ModelAndView("registration");
        if (!form.getPassword().equals(form.getRepeat())) {
            model.addObject("error", "Password and repeat not equals");
            return model;
        }

        final User user = new User();
        user.setLogin(form.getLogin());
        user.setName(form.getName());
        user.setEmail(form.getEmail());
        user.setPhone("+380" + form.getPhone());
        user.setPassword(new BCryptPasswordEncoder().encode(form.getPassword()));

        userService.save(user);

        if (user.getId() == null) {
            model.addObject("error", "User not create");
            return model;
        }
        model.addObject("massage", "User create!");

        return model;
    }
}
