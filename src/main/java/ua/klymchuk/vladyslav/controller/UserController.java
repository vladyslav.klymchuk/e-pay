package ua.klymchuk.vladyslav.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.klymchuk.vladyslav.domain.Payment;
import ua.klymchuk.vladyslav.dto.AccountDTO;
import ua.klymchuk.vladyslav.dto.MapperDTO;
import ua.klymchuk.vladyslav.dto.PaymentDTO;
import ua.klymchuk.vladyslav.dto.UserDTO;
import ua.klymchuk.vladyslav.model.AccountModel;
import ua.klymchuk.vladyslav.model.UserModel;
import ua.klymchuk.vladyslav.service.PaymentService;
import ua.klymchuk.vladyslav.service.UserModelService;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
    private final UserModelService userModelService;
    private final PaymentService paymentService;
    private final MapperDTO mapping;

    @Autowired
    public UserController(UserModelService userModelService, PaymentService paymentService, MapperDTO mapping) {
        this.userModelService = userModelService;
        this.paymentService = paymentService;
        this.mapping = mapping;
    }

    @GetMapping
    public ModelAndView index() {
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final String login;
        if (principal instanceof UserDetails) {
            login = ((UserDetails) principal).getUsername();
        } else {
            login = principal.toString();
        }

        final UserModel userModel =  userModelService.getByLogin(login);
        final UserDTO user = mapping.getUserDTO(userModel);

        final List<AccountDTO> accounts = new ArrayList<>();
        for (AccountModel accountModel : userModel.getAccounts()){
            AccountDTO accountDTO = mapping.getAccountDTO(accountModel);
            accounts.add(accountDTO);
        }

        return new ModelAndView("user").
                addObject("user", user).
                addObject("accounts", accounts);
    }

    @PostMapping
    public ModelAndView list(@RequestParam Integer accountId) {
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final String login;
        if (principal instanceof UserDetails) {
            login = ((UserDetails) principal).getUsername();
        } else {
            login = principal.toString();
        }

        final UserModel userModel =  userModelService.getByLogin(login);
        final UserDTO user = mapping.getUserDTO(userModel);

        final List<AccountDTO> accounts = new ArrayList<>();
        for (AccountModel accountModel : userModel.getAccounts()){
            AccountDTO accountDTO = mapping.getAccountDTO(accountModel);
            accounts.add(accountDTO);
        }

        final List<PaymentDTO> payments = new ArrayList<>();
        for (Payment payment : paymentService.list(accountId)){
            PaymentDTO paymentDTO = mapping.getPaymentDTO(payment);
            payments.add(paymentDTO);
        }

        return new ModelAndView("user").
                addObject("user", user).
                addObject("accounts", accounts).
                addObject("payments", payments);
    }




//    @GetMapping("/password")
//    @ResponseBody
//    public String userPsw(@RequestParam String psw) {
//        System.out.println("userPsw");
//        System.out.println(psw);
//
//        return new BCryptPasswordEncoder().encode(psw);
//    }
}
