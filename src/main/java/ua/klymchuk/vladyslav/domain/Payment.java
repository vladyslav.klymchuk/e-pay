package ua.klymchuk.vladyslav.domain;

import java.time.LocalDate;
import java.util.Objects;

public class Payment extends Domain {
    private int accountId;
    private LocalDate date;
    private double amount;

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Payment payment = (Payment) o;

        if (accountId != payment.accountId) return false;
        if (Double.compare(payment.amount, amount) != 0) return false;
        return Objects.equals(date, payment.date);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = accountId;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        temp = Double.doubleToLongBits(amount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
