package ua.klymchuk.vladyslav.dto;

public class AccountDTO {
    private int id;
    private String name;
    private long numberCard;
    private double amountCard;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getNumberCard() {
        return numberCard;
    }

    public void setNumberCard(long numberCard) {
        this.numberCard = numberCard;
    }

    public double getAmountCard() {
        return amountCard;
    }

    public void setAmountCard(double amountCard) {
        this.amountCard = amountCard;
    }
}
