package ua.klymchuk.vladyslav.dto;

import org.springframework.stereotype.Component;
import ua.klymchuk.vladyslav.domain.Payment;
import ua.klymchuk.vladyslav.model.AccountModel;
import ua.klymchuk.vladyslav.model.UserModel;

@Component
public class MapperDTO {

    public UserDTO getUserDTO(UserModel userModel) {
        final UserDTO userDTO = new UserDTO();
        userDTO.setId(userModel.getUser().getId());
        userDTO.setName(userModel.getUser().getName());
        userDTO.setLogin(userModel.getUser().getLogin());
        userDTO.setEmail(userModel.getUser().getEmail());
        userDTO.setPhone(userModel.getUser().getPhone());

        return userDTO;
    }

    public AccountDTO getAccountDTO(AccountModel accountModel) {
        final AccountDTO accountDTO = new AccountDTO();
        accountDTO.setId(accountModel.getAccount().getId());
        accountDTO.setName(accountModel.getAccount().getName());
        accountDTO.setNumberCard(accountModel.getCard().getNumber());
        accountDTO.setAmountCard(accountModel.getCard().getAmount());

        return accountDTO;
    }

    public PaymentDTO getPaymentDTO(Payment payment) {
        final PaymentDTO paymentDTO = new PaymentDTO();
        paymentDTO.setDate(payment.getDate());
        paymentDTO.setAmount(payment.getAmount());

        return paymentDTO;
    }

}
