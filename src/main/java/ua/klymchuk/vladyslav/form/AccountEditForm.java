package ua.klymchuk.vladyslav.form;

public class AccountEditForm {
    private Integer userId;
    private String nameAccount;
    private long numberCard;
    private double amountCard;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getNameAccount() {
        return nameAccount;
    }

    public void setNameAccount(String nameAccount) {
        this.nameAccount = nameAccount;
    }

    public long getNumberCard() {
        return numberCard;
    }

    public void setNumberCard(long numberCard) {
        this.numberCard = numberCard;
    }

    public double getAmountCard() {
        return amountCard;
    }

    public void setAmountCard(double amountCard) {
        this.amountCard = amountCard;
    }
}
