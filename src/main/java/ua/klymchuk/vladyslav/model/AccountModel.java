package ua.klymchuk.vladyslav.model;

import ua.klymchuk.vladyslav.domain.Account;
import ua.klymchuk.vladyslav.domain.Card;

public class AccountModel {
    private Account account;
    private Card card;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
