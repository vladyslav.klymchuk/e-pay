package ua.klymchuk.vladyslav.model;

import ua.klymchuk.vladyslav.domain.User;

import java.util.List;

public class UserModel {
    private User user;
    private List<AccountModel> accounts;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<AccountModel> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountModel> accounts) {
        this.accounts = accounts;
    }
}
