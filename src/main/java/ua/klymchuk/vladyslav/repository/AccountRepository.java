package ua.klymchuk.vladyslav.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.klymchuk.vladyslav.domain.Account;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

@Repository
public class AccountRepository {
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public AccountRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public Account get(int id) {
        final String sql = "SELECT * FROM accounts WHERE id = :id";

        final List<Account> accounts = namedParameterJdbcTemplate.query(
                sql,
                new MapSqlParameterSource("id", id),
                getRowMapper()
        );

        return accounts.isEmpty() ? null : accounts.get(0);
    }

//    public List<Account> list() {
//        final String sql = "SELECT * FROM accounts";
//
//        return jdbcTemplate.query(
//                sql,
//                getRowMapper()
//        );
//    }

    public List<Account> list(String login) {
        final String sql = "" +
                " SELECT a.* FROM accounts a" +
                " JOIN users u ON u.id = a.user_id" +
                " WHERE u.login = :login";

        return namedParameterJdbcTemplate.query(
                sql,
                new MapSqlParameterSource("login", login),
                getRowMapper()
        );
    }

    public boolean delete(Account account) {
        final String sql = "DELETE FROM Accounts WHERE id = :id";

        final int rezult = namedParameterJdbcTemplate.update(
                sql,
                new MapSqlParameterSource("id", account.getId())
        );

        if (rezult != 0) {
            return true;
        } else {
            return false;
        }
    }

    public void save(Account account) {
        if (account.getId() == null) {
            insert(account);
        } else {
            update(account);
        }
    }

    private void update(Account account) {
        final String sql = "" +
                " UPDATE accounts " +
                " SET \"name\"=:name" +
                " WHERE id = :id";

        namedParameterJdbcTemplate.update(
                sql,
                new MapSqlParameterSource()
                        .addValue("name", account.getName())
                        .addValue("id", account.getId())
        );
    }

    private void insert(Account account) {
        final KeyHolder keyHolder = new GeneratedKeyHolder();
        final String sql = "" +
                " INSERT INTO \"accounts\" (\"user_id\", \"card_id\", \"name\") " +
                " VALUES (:userId, :cardId, :name)";

        namedParameterJdbcTemplate.update(
                sql,
                new MapSqlParameterSource()
                        .addValue("userId", account.getUserId())
                        .addValue("cardId", account.getCardId())
                        .addValue("name", account.getName()),
                keyHolder
        );

        final Integer id = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");
        account.setId(id);
    }

    private RowMapper<Account> getRowMapper() {
        return new RowMapper<>() {
            @Override
            public Account mapRow(ResultSet rs, int rowNum) throws SQLException {
                final Account account = new Account();
                account.setId(rs.getInt("id"));
                account.setCardId(rs.getInt("card_id"));
                account.setUserId(rs.getInt("user_id"));
                account.setName(rs.getString("name"));
                return account;
            }
        };
    }
}
