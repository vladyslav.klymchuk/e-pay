package ua.klymchuk.vladyslav.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.klymchuk.vladyslav.domain.Card;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

@Repository
public class CardRepository {
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public CardRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public Card get(int id) {
        final String sql = "SELECT * FROM cards WHERE id = :id";

        final List<Card> cards = namedParameterJdbcTemplate.query(
                sql,
                new MapSqlParameterSource("id", id),
                getRowMapper()
        );

        return cards.isEmpty() ? null : cards.get(0);
    }

//    public List<Card> list() {
//        final String sql = "SELECT * FROM cards";
//
//        return jdbcTemplate.query(
//                sql,
//                getRowMapper()
//        );
//    }

    public List<Card> list(String login) {
        final String sql = "" +
                " SELECT c.* FROM cards c\n" +
                " JOIN accounts a ON c.id = a.card_id\n" +
                " JOIN users u ON u.id = a.user_id\n" +
                " WHERE u.login = :login";

        return namedParameterJdbcTemplate.query(
                sql,
                new MapSqlParameterSource("login", login),
                getRowMapper()
        );
    }

    public Card getByAccountId(int accountId) {
        final String sql = "" +
                " SELECT c.* FROM cards c" +
                " JOIN accounts a ON c.id = a.card_id" +
                " WHERE a.id = :accountId";

        final List<Card> cards = namedParameterJdbcTemplate.query(
                sql,
                new MapSqlParameterSource("accountId", accountId),
                getRowMapper());

        return cards.get(0);
    }

    public boolean delete(Card card) {
        final String sql = "DELETE FROM cards WHERE id = :id";

        final int rezult = namedParameterJdbcTemplate.update(
                sql,
                new MapSqlParameterSource("id", card.getId())
        );

        if (rezult != 0) {
            return true;
        } else {
            return false;
        }
    }

    public void save(Card card) {
        if (card.getId() == null) {
            insert(card);
        } else {
            update(card);
        }
    }

    private void update(Card card) {
        final String sql = "" +
                " UPDATE cards " +
                " SET \"number\"=:number, \"amount\"=:amount" +
                " WHERE id = :id";

        namedParameterJdbcTemplate.update(
                sql,
                new MapSqlParameterSource()
                        .addValue("number", card.getNumber())
                        .addValue("amount", card.getAmount())
                        .addValue("id", card.getId())
        );
    }

    private void insert(Card card) {
        final KeyHolder keyHolder = new GeneratedKeyHolder();
        final String sql = "" +
                " INSERT INTO cards (\"number\", \"amount\") " +
                " VALUES (:number, :amount)";

        namedParameterJdbcTemplate.update(
                sql,
                new MapSqlParameterSource()
                        .addValue("number", card.getNumber())
                        .addValue("amount", card.getAmount()),
                keyHolder
        );

        final Integer id = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");
        card.setId(id);
    }

    private RowMapper<Card> getRowMapper() {
        return new RowMapper<>() {
            @Override
            public Card mapRow(ResultSet rs, int rowNum) throws SQLException {
                final Card card = new Card();
                card.setId(rs.getInt("id"));
                card.setNumber(rs.getLong("number"));
                card.setAmount(rs.getDouble("amount"));

                return card;
            }
        };
    }
}
