package ua.klymchuk.vladyslav.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.klymchuk.vladyslav.domain.Payment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

@Repository
public class PaymentRepository {
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public PaymentRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public Payment get(int id) {
        final String sql = "SELECT * FROM payments WHERE id = :id";

        final List<Payment> payments = namedParameterJdbcTemplate.query(
                sql,
                new MapSqlParameterSource("id", id),
                getRowMapper()
        );

        return payments.isEmpty() ? null : payments.get(0);
    }

//    public List<Payment> list() {
//        final String sql = "SELECT * FROM payments";
//
//        return jdbcTemplate.query(
//                sql,
//                getRowMapper()
//        );
//    }

    public List<Payment> list(String login) {
        final String sql = "" +
                " SELECT p.* FROM payments p" +
                " JOIN accounts a on p.id = a.id" +
                " JOIN users u on u.id = a.user_id" +
                " WHERE u.login = :login";

        return namedParameterJdbcTemplate.query(
                sql,
                new MapSqlParameterSource("login", login),
                getRowMapper()
        );
    }

    public List<Payment> list(int accountId) {
        final String sql = "" +
                " SELECT p.* FROM payments p" +
                " WHERE p.account_id = :accountId";

        return namedParameterJdbcTemplate.query(
                sql,
                new MapSqlParameterSource("accountId", accountId),
                getRowMapper()
        );
    }

    public boolean delete(Payment payment) {
        final String sql = "DELETE FROM payments WHERE id = :id";

        final int rezult = namedParameterJdbcTemplate.update(
                sql,
                new MapSqlParameterSource("id", payment.getId())
        );

        if (rezult != 0) {
            return true;
        } else {
            return false;
        }
    }

    public void save(Payment payment) {
        if (payment.getId() == null) {
            insert(payment);
        } else {
            update(payment);
        }
    }

    private void update(Payment payment) {
        final String sql = "" +
                " UPDATE payments " +
                " SET \"date\"=:date, \"amount\"=:amount" +
                " WHERE id = :id";

        namedParameterJdbcTemplate.update(
                sql,
                new MapSqlParameterSource()
                        .addValue("date", payment.getDate())
                        .addValue("amount", payment.getAmount())
                        .addValue("id", payment.getId())
        );
    }

    private void insert(Payment payment) {
        final KeyHolder keyHolder = new GeneratedKeyHolder();
        final String sql = "" +
                " INSERT INTO \"payments\" (\"account_id\", \"date\", amount) " +
                " VALUES (:accountId, :date, :amount)";

        namedParameterJdbcTemplate.update(
                sql,
                new MapSqlParameterSource()
                        .addValue("accountId", payment.getAccountId())
                        .addValue("date", payment.getDate())
                        .addValue("amount", payment.getAmount()),
                keyHolder
        );

        final Integer id = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");
        payment.setId(id);
    }

    private RowMapper<Payment> getRowMapper() {
        return new RowMapper<>() {
            @Override
            public Payment mapRow(ResultSet rs, int rowNum) throws SQLException {
                final Payment payment = new Payment();
                payment.setId(rs.getInt("id"));
                payment.setDate(rs.getDate("date").toLocalDate());
                payment.setAmount(rs.getDouble("amount"));

                return payment;
            }
        };
    }
}
