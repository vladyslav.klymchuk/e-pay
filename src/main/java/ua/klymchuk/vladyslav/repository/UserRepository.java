package ua.klymchuk.vladyslav.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.klymchuk.vladyslav.domain.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

@Repository
public class UserRepository {
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public UserRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public User get(int id) {
        final String sql = " SELECT * FROM users WHERE id = :id";

        final List<User> users = namedParameterJdbcTemplate.query(
                sql,
                new MapSqlParameterSource("id", id),
                getRowMapper()
        );

        return users.isEmpty() ? null : users.get(0);
    }

    public User getByLogin(String login) {
        final String sql = " SELECT * FROM users WHERE login = :login";

        final List<User> users = namedParameterJdbcTemplate.query(
                sql,
                new MapSqlParameterSource("login", login),
                getRowMapper()
        );

        return users.isEmpty() ? null : users.get(0);
    }

    public List<User> list() {
        final String sql = " SELECT * FROM users";

        return jdbcTemplate.query(
                sql,
                getRowMapper()
        );
    }

    public boolean delete(User user) {
        final String sql = " DELETE FROM users WHERE id = :id";

        final int rezult = namedParameterJdbcTemplate.update(
                sql,
                new MapSqlParameterSource("id", user.getId())
        );

        if (rezult != 0){
            return true;
        } else {
            return false;
        }
    }
    public void save(User user) {
        if (user.getId() == null) {
            insert(user);
        } else {
            update(user);
        }
    }

    private void update(User user) {
        final String sql = "" +
                " UPDATE \"users\"" +
                " SET \"login\"=:login," +
                "    \"password\"=:password," +
                "    \"name\"=:name," +
                "    \"phone_number\"=:phone," +
                "    \"email\"=:email," +
                "    \"available\"=:available" +
                " WHERE id = :id";

        namedParameterJdbcTemplate.update(
                sql,
                getMapSqlParameterSource(user)
        );
    }

    private void insert(User user) {
        final KeyHolder keyHolder = new GeneratedKeyHolder();
        final String sql = "" +
                " INSERT INTO \"users\" (\"login\", \"password\", \"name\", \"phone_number\", \"email\")" +
                " VALUES (:login, :password, :name, :phone, :email)";


        namedParameterJdbcTemplate.update(
                sql,
                getMapSqlParameterSource(user),
                keyHolder
        );

        final Integer id = (Integer) Objects.requireNonNull(keyHolder.getKeys()).get("id");
        user.setId(id);
    }


    private MapSqlParameterSource getMapSqlParameterSource(User user){
        return new MapSqlParameterSource()
                .addValue("login", user.getLogin())
                .addValue("password", user.getPassword())
                .addValue("name", user.getName())
                .addValue("phone", user.getPhone())
                .addValue("email", user.getEmail())
                .addValue("available", user.getAvailable())
                .addValue("id", user.getId());
    }

    private RowMapper<User> getRowMapper() {
        return new RowMapper<>() {
            @Override
            public User mapRow(ResultSet rs, int rowNum) throws SQLException {
                final User user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
                user.setPassword(rs.getString("password"));
                user.setName(rs.getString("name"));
                user.setPhone(rs.getString("phone_number"));
                user.setEmail(rs.getString("email"));
                user.setAvailable(rs.getBoolean("available"));

                return user;
            }
        };
    }

}
