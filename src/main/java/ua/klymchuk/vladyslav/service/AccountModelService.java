package ua.klymchuk.vladyslav.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.klymchuk.vladyslav.domain.Account;
import ua.klymchuk.vladyslav.domain.Card;
import ua.klymchuk.vladyslav.domain.Payment;
import ua.klymchuk.vladyslav.model.AccountModel;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountModelService {
    private final AccountService accountService;
    private final CardService cardService;

    @Autowired
    public AccountModelService(AccountService accountService, CardService cardService, PaymentService paymentService) {
        this.accountService = accountService;
        this.cardService = cardService;
    }

    public List<AccountModel> get(String userLogin) {
        final List<Account> accounts = accountService.list(userLogin);
        final List<Card> cards = cardService.list(userLogin);

        final List<AccountModel> accountModels = new ArrayList<>();

        for (Account account : accounts) {
            final AccountModel accountModel = new AccountModel();
            accountModel.setAccount(account);

            final Card card = getCardById(account.getCardId(), cards);
            accountModel.setCard(card);

            accountModels.add(accountModel);
        }

        return accountModels;
    }

    private Card getCardById(int id, List<Card> cards) {
        for (Card card : cards) {
            if (card.getId() == id) {
                return card;
            }
        }

        return null;
    }
}
