package ua.klymchuk.vladyslav.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.klymchuk.vladyslav.domain.Account;
import ua.klymchuk.vladyslav.domain.Card;
import ua.klymchuk.vladyslav.form.AccountEditForm;
import ua.klymchuk.vladyslav.repository.AccountRepository;
import ua.klymchuk.vladyslav.repository.CardRepository;


import java.util.List;

@Service
public class AccountService {
    private final AccountRepository accountRepository;
    private final CardRepository cardRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository, CardRepository cardRepository) {
        this.accountRepository = accountRepository;
        this.cardRepository = cardRepository;
    }

    public Account get(int id) {
        return accountRepository.get(id);
    }

    public List<Account> list(String userLogin) {
        return accountRepository.list(userLogin);
    }

    public void save(AccountEditForm form) {
        final Card card = new Card();
        card.setNumber(form.getNumberCard());
        card.setAmount(form.getAmountCard());
        cardRepository.save(card);

        if (card.getId() == null) {
            return;
        }
        final Account account = new Account();
        account.setUserId(form.getUserId());
        account.setName(form.getNameAccount());
        account.setCardId(card.getId());

        accountRepository.save(account);
    }
}
