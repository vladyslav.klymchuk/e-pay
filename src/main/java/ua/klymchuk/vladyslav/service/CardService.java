package ua.klymchuk.vladyslav.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.klymchuk.vladyslav.domain.Card;
import ua.klymchuk.vladyslav.repository.CardRepository;

import java.util.List;

@Service
public class CardService {
    private final CardRepository repository;

    @Autowired
    public CardService(CardRepository repository) {
        this.repository = repository;
    }

    public Card get(int id) {
        return repository.get(id);
    }

    public Card getByAccountId(int accountId) {
        return repository.getByAccountId(accountId);
    }

    public List<Card> list(String username) {
        return repository.list(username);
    }

    public void save(Card card) {
        repository.save(card);
    }
}
