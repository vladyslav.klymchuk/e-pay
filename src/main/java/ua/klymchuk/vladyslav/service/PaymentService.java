package ua.klymchuk.vladyslav.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.klymchuk.vladyslav.domain.Card;
import ua.klymchuk.vladyslav.domain.Payment;
import ua.klymchuk.vladyslav.form.PaymentRegistrationForm;
import ua.klymchuk.vladyslav.repository.PaymentRepository;

import java.time.LocalDate;
import java.util.List;

@Service
public class PaymentService {
    private final PaymentRepository paymentRepository;
    private final CardService cardService;

    @Autowired
    public PaymentService(PaymentRepository paymentRepository, CardService cardService) {
        this.paymentRepository = paymentRepository;
        this.cardService = cardService;
    }

    public Payment get(int id) {
        return paymentRepository.get(id);
    }

    public List<Payment> list(String login) {
        return paymentRepository.list(login);
    }

    public List<Payment> list(int accountId) {
        return paymentRepository.list(accountId);
    }

    @Transactional
    public boolean save(PaymentRegistrationForm form) {
        final Card card = cardService.getByAccountId(form.getAccountId());
        if (card.getAmount() < form.getAmount()) {
            return false;
        }

        final Payment payment = new Payment();
        payment.setAccountId(form.getAccountId());
        payment.setDate(LocalDate.now());
        payment.setAmount(form.getAmount());
        card.setAmount(card.getAmount() - form.getAmount());

        cardService.save(card);
        paymentRepository.save(payment);

        return true;
    }
}
