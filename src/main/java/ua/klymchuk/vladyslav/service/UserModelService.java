package ua.klymchuk.vladyslav.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.klymchuk.vladyslav.domain.User;
import ua.klymchuk.vladyslav.model.AccountModel;
import ua.klymchuk.vladyslav.model.UserModel;

import java.util.List;

@Service
public class UserModelService {
    private final UserService userService;
    private final AccountModelService accountService;

    @Autowired
    public UserModelService(UserService userService, AccountModelService accountService) {
        this.userService = userService;
        this.accountService = accountService;
    }

    public UserModel getByLogin(String username){
        final User user = userService.getByLogin(username);
        final List<AccountModel> accounts = accountService.get(username);

        final UserModel userModel = new UserModel();
        userModel.setUser(user);
        userModel.setAccounts(accounts);

        return userModel;
    }
}
