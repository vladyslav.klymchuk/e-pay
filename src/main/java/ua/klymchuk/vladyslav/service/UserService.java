package ua.klymchuk.vladyslav.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.klymchuk.vladyslav.domain.User;
import ua.klymchuk.vladyslav.repository.UserRepository;

import java.util.List;

@Service
public class UserService {
    private final UserRepository repository;

    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public User get(int id) {
        return repository.get(id);
    }

    public User getByLogin(String login) {
        return repository.getByLogin(login);
    }

    public List<User> list() {
        return repository.list();
    }

    public void save(User user) {
        if (user.getId() == null) {
            final User existUser = getByLogin(user.getLogin());
            if (existUser == null) {
                repository.save(user);
            }
        } else {
            repository.save(user);
        }
    }
}
