DROP TABLE IF EXISTS "accounts";

CREATE TABLE IF NOT EXISTS "accounts"
(
    id           SERIAL  NOT NULL,
    user_id      INTEGER NOT NULL REFERENCES "users" (id) ON DELETE CASCADE,
    card_id      INTEGER NOT NULL REFERENCES "cards" (id) ON DELETE CASCADE,
    name         VARCHAR(100) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE(user_id, card_id)
);