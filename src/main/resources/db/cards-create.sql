DROP TABLE IF EXISTS "cards";

CREATE TABLE IF NOT EXISTS "cards"
(
    id           SERIAL         NOT NULL,
    number       NUMERIC(16, 0) NOT NULL UNIQUE,
    amount       NUMERIC(10, 2) DEFAULT 0 NOT NULL,
    PRIMARY KEY (id)
);