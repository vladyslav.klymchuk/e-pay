DROP TABLE IF EXISTS "users";

CREATE TABLE IF NOT EXISTS "users"
(
    id           SERIAL       NOT NULL,
    login        VARCHAR(250) NOT NULL UNIQUE,
    password     VARCHAR(100) NOT NULL,
    name         VARCHAR(100) NOT NULL,
    phone_number VARCHAR(100) NOT NULL UNIQUE,
    email        VARCHAR(100) NOT NULL UNIQUE,
    available    BOOLEAN      NOT NULL DEFAULT TRUE,
    role         VARCHAR(10)  NOT NULL DEFAULT 'USER',
    PRIMARY KEY (id)
);