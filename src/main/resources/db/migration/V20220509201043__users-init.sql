INSERT INTO "users" (login, password, name, phone_number, email)
VALUES ('admin', '$2a$10$MVrGqZBrhkFz9htzdZuVwujmVAHF7sVN0B8yi0qj6kQj7.j8s0ebO', 'Admin', '011-111-11-11',
        'admin@e-pay.com'),
       ('manager', '$2a$10$b01gMW6ovRB.wSNQ5Bqbb.tKL/GTR0F.lwZ1vF4z/JOWvYgrQfi2q', 'Manager', '022-222-22-22',
        'manager@e-pay.com'),
       ('user', '$2a$10$cC0hxIh4AhxrXWVbu3s1ZemTU.hlntkdzE.kgg0YV8yVkWlDTsZFS', 'Vlad', '067-198-09-96',
        'klim23@gmail.com');