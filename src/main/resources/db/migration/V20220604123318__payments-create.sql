CREATE TABLE IF NOT EXISTS "payments"
(
    id           SERIAL  NOT NULL,
    account_id   INTEGER NOT NULL REFERENCES "accounts" (id) ON DELETE CASCADE,
    date         DATE    NOT NULL,
    amount       NUMERIC(10, 2) DEFAULT 0 NOT NULL,
    PRIMARY KEY (id)
);